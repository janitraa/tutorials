1. Scene BlueShip dan StonePlatform sama-sama memiliki sebuah child node bertipe Sprite. Apa fungsi dari node bertipe Sprite?

Node bertipe Sprite berfungsi sebagai node yang dapat menunjukan texture 2D. Texture tersebut berupa area dari Atlas Texture yang lebih besar atau frame dari animasi Sprite Sheet.


2. Root node dari scene BlueShip dan StonePlatform menggunakan tipe yang berbeda. BlueShip menggunakan tipe RigidBody2D, sedangkan StonePlatform menggunakan tipe StaticBody2D. Apa perbedaan dari masing-masing tipe node?

Tipe RigidBody2D yang digunakan BlueShip berfungsi untuk membuat body yang di kontrol oleh physical engine Godot. Sprite tersebut dapat dipengaruhi oleh nilai gravitasi dan dapat dikontrol dengan menggunakan script. Sementara, tipe StaticBody2D yang digunakan StonePlatform berfungsi untuk membuat body yang statis (tidak dapat bergerak). StaticBody2D memiliki beberapa atribut, seperti bounce, friction atau velocity, dan biasa digunakan untuk membuat object environment seperti tanah atau dinding.


3. Ubah nilai atribut Mass dan Weight pada tipe RigidBody2D secara bebas di scene BlueShip, lalu coba jalankan scene Main. Apa yang terjadi?

Karena Weight atau berat yang ditambahkan maka node BlueShip akan terjatuh lebih cepat sesuai dengan ilmu fisika dimana apabila objek tersebut menjadi lebih berat.


4. Ubah nilai atribut Disabled pada tipe CollisionShape2D di scene StonePlatform, lalu coba jalankan scene Main. Apa yang terjadi?

Objek StonePlatform tidak akan dapat berinterikasi dengan objek lainnya yang pasif. Objek tersebut tidak akan dapat ditabrak oleh objek lainnya, contohnya objek yang berjatuhan tidak akan dianggap sebagai objek yang dapat ditrabak.


5. Pada scene Main, coba manipulasi atribut Position, Rotation, dan Scale milik node BlueShip secara bebas. Apa yang terjadi pada visualisasi BlueShip di Viewport?

Ketika atribut Position diubah, posisi node BlueShip berubah. Position menentukan lokasi dimana node berada.
Ketika atribut Rotation diubah, node BlueShip berputar. Rotation dapat memutar asset yang digunakan untuk node.
Ketika atribut Scale diubah, ukuran dari node BlueShip akan berubah. Scale berguna untuk membesar kecilkan node.


6. Pada scene Main, perhatikan nilai atribut Position node PlatformBlue, StonePlatform, dan StonePlatform2. Mengapa nilai Position node StonePlatform dan StonePlatform2 tidak sesuai dengan posisinya di dalam scene (menurut Inspector) namun visualisasinya berada di posisi yang tepat?

Nilai Position node StonePlatform dan StonePlatform2 tidak sesuai karena nilai Position pada node PlatformBlue merupakan letak dari grup atau node parent dari StonePlatform dan StonePlatform2.